module BootsProductsHelper

  def get_rate
    rate_str = Preference.where(key: 'gbp_to_cny_rate').first.value
    rate_str.to_f
  end

end
