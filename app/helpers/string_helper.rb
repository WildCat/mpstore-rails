module StringHelper

  def self.match_first(regex, str)
    m = regex.match str
    return m ? m[1] : m
  end

end
