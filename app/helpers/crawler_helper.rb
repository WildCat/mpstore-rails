module CrawlerHelper

  @agent = Mechanize.new
  # @agent.user_agent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4'

  def self.get_appletuan_list(page: nil)
    url = 'http://www.appletuan.com/go/dailyprice'
    url += "?p=#{page}" unless page.nil?
    @agent.get url
  end

  def self.get_appletuan_topic(uri)
    @agent.get 'http://www.appletuan.com/' + uri
  end

  def self.get_price_history(url)
    @agent.get 'http://tool.manmanbuy.com/history.aspx?w=950&h=580&h2=360&m=1&e=1&tofanli=1&url=' + url
  end

  def self.get_boots_item(url)
    @agent.get url
  end

end
