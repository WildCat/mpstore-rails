json.extract! boots_product, :id, :created_at, :updated_at
json.url boots_product_url(boots_product, format: :json)