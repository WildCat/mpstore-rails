class BootsProductsController < ApplicationController
  before_action :set_boots_product, only: [:show, :edit, :update, :destroy]

  # GET /boots_products
  # GET /boots_products.json
  def index
    @boots_products = BootsProduct.all
  end

  # GET /boots_products/1
  # GET /boots_products/1.json
  def show
  end

  # GET /boots_products/new
  def new
    @boots_product = BootsProduct.new
  end

  # GET /boots_products/1/edit
  def edit
  end

  # POST /boots_products
  # POST /boots_products.json
  def create
    @boots_product = BootsProduct.new(boots_product_params)

    respond_to do |format|
      if @boots_product.save
        format.html { redirect_to @boots_product, notice: 'Boots product was successfully created.' }
        format.json { render :show, status: :created, location: @boots_product }
      else
        format.html { render :new }
        format.json { render json: @boots_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /boots_products/1
  # PATCH/PUT /boots_products/1.json
  def update
    respond_to do |format|
      if @boots_product.update(boots_product_params)
        format.html { redirect_to @boots_product, notice: 'Boots product was successfully updated.' }
        format.json { render :show, status: :ok, location: @boots_product }
      else
        format.html { render :edit }
        format.json { render json: @boots_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /boots_products/1
  # DELETE /boots_products/1.json
  def destroy
    @boots_product.destroy
    respond_to do |format|
      format.html { redirect_to boots_products_url, notice: 'Boots product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_boots_product
      @boots_product = BootsProduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def boots_product_params
      params.fetch(:boots_product, {})
    end
end
