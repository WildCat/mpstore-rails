class FetchWorker
  include Sidekiq::Worker

  def perform
    [
        'https://www.boots.com/en/Boots-Essentials-Cucumber-Facial-Toner-150ml_982447/',
        'https://www.boots.com/en/Boots-Essentials-Cucumber-3-Minute-Clay-Mask-50ml_869647/',
        'https://www.boots.com/en/Boots-Essentials-Cucumber-Moisturising-Cream-1-x-100ml_869591/',
    ].each do |url|
      BootsItemWorker.perform_async url
    end
  end
end
