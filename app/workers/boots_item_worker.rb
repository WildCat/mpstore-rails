class BootsItemWorker
  include Sidekiq::Worker

  def perform(boots_url)
    # agent = Mechanize.new
    # web_page = agent.get boots_url
    web_page = CrawlerHelper.get_boots_item boots_url
    html_doc = Nokogiri::HTML(web_page.content)

    product_id = html_doc.at_css('input#productId[type="hidden"]')['value'].to_i

    product_name = html_doc.at_css('.pd_productNameSpan').text
    price = StringHelper.match_first(/£(\d+\.\d+)/, html_doc.at_css('.productOfferPrice').text)
    price = price.to_f if price

    # Parse image URL
    image_url = html_doc.at_css('meta[property="og:image"]')['content']
    puts "Product name(#{product_id}): #{product_name} , Price: #{price} , image URL: #{image_url}"

    bp = BootsProduct.where(product_id: product_id).first_or_initialize
    bp.name = product_name
    bp.price = price
    bp.image_url = image_url
    bp.save
  end
end
