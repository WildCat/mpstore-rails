require 'test_helper'

class BootsProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @boots_product = boots_products(:one)
  end

  test "should get index" do
    get boots_products_url
    assert_response :success
  end

  test "should get new" do
    get new_boots_product_url
    assert_response :success
  end

  test "should create boots_product" do
    assert_difference('BootsProduct.count') do
      post boots_products_url, params: { boots_product: {  } }
    end

    assert_redirected_to boots_product_url(BootsProduct.last)
  end

  test "should show boots_product" do
    get boots_product_url(@boots_product)
    assert_response :success
  end

  test "should get edit" do
    get edit_boots_product_url(@boots_product)
    assert_response :success
  end

  test "should update boots_product" do
    patch boots_product_url(@boots_product), params: { boots_product: {  } }
    assert_redirected_to boots_product_url(@boots_product)
  end

  test "should destroy boots_product" do
    assert_difference('BootsProduct.count', -1) do
      delete boots_product_url(@boots_product)
    end

    assert_redirected_to boots_products_url
  end
end
