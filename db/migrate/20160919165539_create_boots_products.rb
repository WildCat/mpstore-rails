class CreateBootsProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :boots_products do |t|
      t.integer :product_id, null: false, unique: true
      t.string :name, null: false
      t.string :zh_name
      t.string :image_url, null: false
      t.float :price, null: false
      t.float :fixed_price
      t.float :cny_cost
      t.float :cny_price

      t.timestamps
    end
  end
end
